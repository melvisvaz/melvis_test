﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer cust = new Customer("Peter");
            cust.OpenAccount(new CheckingAccount(11));
            bank.AddCustomer(cust);

            Assert.AreEqual("Customer Summary\n - Peter (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void TotalInterestPaidMultipleCustomers()
        {
            Bank bank = new Bank();
            Customer cust = new Customer("Peter");
            Account checkingAccount = new CheckingAccount(12);
            cust.OpenAccount(checkingAccount);
            bank.AddCustomer(cust);

            checkingAccount.Deposit(15000.0);

            cust = new Customer("Jack");
            Account savingAccount = new SavingAccount(12);
            cust.OpenAccount(savingAccount);
            bank.AddCustomer(cust);

            savingAccount.Deposit(2000.0);

            Assert.AreEqual(17.0, bank.TotalInterestPaid());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount(12);
            Customer bill = new Customer("Jack").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(50.0);

            Assert.AreEqual(0.05, bank.TotalInterestPaid());
        }

        [Test]
        public void SavingsAccountTestCase1()
        {
            Bank bank = new Bank();
            Account checkingAccount = new SavingAccount(12);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(1.0, bank.TotalInterestPaid());
        }

        [Test]
        public void SavingsAccountTestCase2()
        {
            Bank bank = new Bank();
            Account checkingAccount = new SavingAccount(11);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1000.0);

            Assert.AreEqual(1.0, bank.TotalInterestPaid());
        }

        [Test]
        public void MaxiSavingsAccountCase1()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount(11);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(100.0, bank.TotalInterestPaid());
        }

        [Test]
        public void MaxiSavingsAccountCase2()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount(11);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1000.0);

            Assert.AreEqual(20.0, bank.TotalInterestPaid());
        }

        [Test]
        public void MaxiSavingsAccountCase3()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount(11);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(2000.0);

            Assert.AreEqual(50.0, bank.TotalInterestPaid());
        }

        [Test]
        public void MaxiSavingsAccountCase4()
        {
            Bank bank = new Bank();
            Account checkingAccount = new MaxiSavingsAccount(12);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(2000.0, DateTime.Now.AddDays(-12));

            Assert.AreEqual(100.0, bank.TotalInterestPaid());
        }
    }
}
