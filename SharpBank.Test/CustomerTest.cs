﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new CheckingAccount(11);
            Account savingsAccount = new SavingAccount(12);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }


        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingAccount(11));
            oscar.OpenAccount(new CheckingAccount(12));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TransferMoneyTestCases()
        {
            var savingsAccount = new SavingAccount(11);
            savingsAccount.Deposit(1000);

            var checkingAccount = new CheckingAccount(12);

            Customer cust = new Customer("James").OpenAccount(savingsAccount);
            cust.OpenAccount(checkingAccount);

            try
            {
                cust.TransferMoney(null, checkingAccount, 200);
                Assert.Fail("'From' Account null - ArgumentNullException not thrown");
            }
            catch (ArgumentNullException)
            {
                // nothing to do
            }

            try
            {
                cust.TransferMoney(savingsAccount, null, 200);
                Assert.Fail("'To' Account null - ArgumentNullException not thrown");
            }
            catch (ArgumentNullException)
            {
                // nothing to do
            }

            try
            {
                cust.TransferMoney(savingsAccount, checkingAccount, -200);
                Assert.Fail("Negative amount - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            try
            {
                cust.TransferMoney(savingsAccount, new MaxiSavingsAccount(-1), 200);
                Assert.Fail("Invalid account - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            try
            {
                cust.TransferMoney(new MaxiSavingsAccount(-1), savingsAccount, 200);
                Assert.Fail("Invalid account - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }


            cust.TransferMoney(savingsAccount, checkingAccount, 700);
            cust.TransferMoney(savingsAccount, checkingAccount, 200);
            Assert.AreEqual(savingsAccount.SumTransactions(), 100, "Savings Account - invalid result");
            Assert.AreEqual(checkingAccount.SumTransactions(), 900, "Checking Account - invalid result");

        }

    }
}
