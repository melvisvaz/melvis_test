﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public abstract class Account
    {
        public int AccountID { get; set; }
        public List<Transaction> Transactions;

        public Account()
        {
            this.Transactions = new List<Transaction>();
        }


        public void Deposit(double amount, DateTime date)
        {
            DepositAmount(amount, date);
        }

        public void Deposit(double amount)
        {
            DepositAmount(amount, null);
        }

        private void DepositAmount(double amount, DateTime? date)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if (date != null)
                {
                    Transactions.Add(new Transaction(amount, date.Value));
                }
                else
                {
                    Transactions.Add(new Transaction(amount));
                }
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                Transactions.Add(new Transaction(-amount));
            }
        }

    
        public double SumTransactions()
        {
            return Transactions.Sum(x => x.Amount);
        }

        public abstract double InterestEarned();
        public abstract int GetAccountType();

    }
}
