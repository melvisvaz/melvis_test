﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("Customer");
            customers.Add(customer);
        }

        public string CustomerSummary()
        {
            var summary = new StringBuilder();
            summary.Append("Customer Summary");
            foreach (Customer c in customers)
                summary.Append("\n - " + c.GetName() + " (" + Format(c.GetNumberOfAccounts(), "account") + ")");
            return summary.ToString();
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private String Format(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            return customers.Sum(customer => customer.TotalInterestEarned());
        }

        public String GetFirstCustomer()
        {
            try
            {
                Customer customer = customers.FirstOrDefault();
                return customer == null ? string.Empty : customer.GetName();
            }
            catch
            {
                throw;
            }
        }
    }
}
