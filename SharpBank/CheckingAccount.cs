﻿namespace SharpBank
{
    public class CheckingAccount : Account
    {
        public const int CHECKING = 0;
        public override int GetAccountType()
        {
            return CHECKING;
        }

        public CheckingAccount(int accountID)
        {
            AccountID = accountID;
        }

        public override string ToString()
        {
            return "Checking Account\n";
        }


        public override double InterestEarned()
        {
            double amt = SumTransactions();
            return amt * 0.001;
        }
    }
}
