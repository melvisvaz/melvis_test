﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            StringBuilder statement = new StringBuilder();

            statement.Append("Statement for " + name + "\n");
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement.Append("\n" + StatementForAccount(a) + "\n");
                total += a.SumTransactions();
            }
            statement.Append("\nTotal In All Accounts " + ToDollars(total));
            return statement.ToString();
        }

        private string StatementForAccount(Account a)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(a.ToString());

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.Transactions)
            {
                sb.Append("  " + (t.Amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.Amount) + "\n");
                total += t.Amount;
            }
            sb.Append("Total " + ToDollars(total));
            return sb.ToString();
        }

        public void TransferMoney(Account fromAccount, Account toAccount, double amount)
        {
            if (fromAccount == null)
            {
                throw new ArgumentNullException("From Account");
            }
            if (toAccount == null)
            {
                throw new ArgumentNullException("To Account");
            }
            if (amount <= 0)
            {
                throw new ArgumentException(string.Format("Invalid amount {0}", amount));
            }
            if (fromAccount.AccountID <= 0)
            {
                throw new ArgumentNullException("From Account is not valid");
            }
            if (toAccount.AccountID <= 0)
            {
                throw new ArgumentNullException("To Account is not valid");
            }

            fromAccount.Withdraw(amount);       
            toAccount.Deposit(amount);
        }

        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
