﻿using System;
using System.Linq;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {
        public const int MAXI_SAVINGS = 2;
        public override int GetAccountType()
        {
            return MAXI_SAVINGS;
        }

        public MaxiSavingsAccount(int accountID)
        {
            AccountID = accountID;
        }

        public override string ToString()
        {
            return "Maxi Savings Account\n";
        }

        public override double InterestEarned()
        {
            double dAmount = SumTransactions();
            if (WithdrawInLast10Days())
            {
                return dAmount * 0.05;
            }
            if (dAmount <= 1000)
            {
                return dAmount * 0.02;
            }
            if (dAmount <= 2000)
            {
                return (dAmount - 1000) * 0.05;
            }
          
            return (dAmount - 2000) * 0.1;
        }

        private bool WithdrawInLast10Days()
        {
            var sum = Transactions.Where(x => 
                            x.TransactionDate >= DateTime.Now.AddDays(-10) &&
                            x.TransactionDate <= DateTime.Now).Select(x => x.Amount).Sum();
            return sum == 0 ? true : false;
        } 
    }
}
