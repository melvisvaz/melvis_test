﻿namespace SharpBank
{
    public class SavingAccount : Account
    {
        public const int SAVINGS = 1;
        public override int GetAccountType()
        {
            return SAVINGS;
        }

        public SavingAccount(int accountID)
        {
            AccountID = accountID;
        }


        public override string ToString()
        {
            return "Savings Account\n";
        }

        public override double InterestEarned()
        {
            double amt = SumTransactions();
            if (amt <= 1000)
            {
                return amt * 0.001;
            }
            return (amt - 1000) * 0.002;
        }
    }
}
    